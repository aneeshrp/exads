# Response to the Exads Candidate Assessement Test #

This script has been developed as a response to the PHP candidate assessment test conducted by Exads.  Test details sent by Exads on 10th Jan 2020, response with answers sent on 14th Jan 2020  

### Author Details ###

* Name: Aneesh Ramakrishna Pillai
* Email: aneeshrp@gmail.com
* Domain: http://aneeshrp.wordpress.com

### General Notes ###

* I have used Sublime as code editor with 4 Tab indentation
* Environment: Laragon ( with PHP7) on a Windows 10 Machine
* Output is tested on a Chrome Browser.
* I have attempted to keep all the functions to work stand-alone, hence instead of return I used echo to print the output in each method.
* I do not prefer using string concatenation along with echo, so that usage is completely ommited.
* All the comments that added as method description and the inline comments are valid, I request you to read them. 
* I didn't calculate how much time it took for me to finish all the tasks.  However task number 4 was bit challenging among all the given tasks.
