<?php
/**
 * Author: Aneesh Ramakrishna Pillai
 * Email: aneeshrp@gmail.com
 * Domain: http://aneeshrp.wordpress.com
 * 
 * This script has been developed as a response to the PHP candidate assessment test conducted by Exads.
 * Test details sent by Exads on 10th Jan 2020, response with answers sent on 14th Jan 2020
 * 
 * General Notes
 * 1.  I have used Sublime as code editor with 4 Tab indentation
 * 2.  Environment: Laragon ( with PHP7) on a Windows 10 Machine
 * 3.  Output is tested on a Chrome Browser.
 * 4.  I have attempted to keep all the functions to work stand-alone, hence instead of 
 *     return I used echo to print the output in each method.
 * 5.  I do not prefer using string concatenation along with echo, so that usage is completely ommited.
 * 6.  All the comments that added as method description and the inline comments are valid, I request you to 
 *     read them. 
 * 7.  I didn't calculate how much time it took for me to finish all the tasks.  However task number 4 was 
 *     bit challenging among all the given tasks.
 **/


class ExadsDevPhpTest {


	/**
	 * 1. FizzBuzz 
	 * Write a PHP script that prints all integer values from 1 to 100. 
	 * 
	 * For multiples of three output "Fizz" instead of the value and for the 
	 * multiples of five output "Buzz".  Values which are multiples of both 
	 * three and five should output as "FizzBuzz". 
	 *
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ This is a typical FizzBuzz solution we do normally.  
	 */

	public function runFizzBuzz(){

		$outputData = '';

		for ($i = 1; $i <= 100; $i++) {
			if ($i%3 == 0 && $i%5 == 0) {
				$outputData 	= 'FizzBuzz';
			} elseif ($i%3 == 0) {
				$outputData 	= 'Fizz';
			} elseif ($i%5 == 0) {
				$outputData 	= 'Buzz';
			} elseif ($i%3 != 0 && $i%5 != 0) {
				$outputData 	= $i;
			} else {
				$outputData 	= 'Invalid FizzBuzz attempt';
			}
			echo $outputData, "<br>";         
		}		
	}

	/**
	 * 1. FizzBuzz 
	 * 
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ A more neat and clean approach with less number of codes.
	 */
	public function runFizzBuzzImproved($n){
		echo ($n % 15 === 0) ? "fizzbuzz" : (( $n % 5 === 0 ) ? "buzz" : (($n % 3 === 0) ? "fizz" : $n )), "<br>";
	}


	/**
	 * 2. 500 Element Array 
	 * Write a PHP script to generate a random array of 500 integers (values of 1 – 500 inclusive). 
	 *
	 * Randomly remove and discard an arbitary element from this newly generated array.  
	 * Write the code to efficiently determine the value of the missing element.  
	 * Explain your reasoning with comments. 
	 *
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ Adding two different solutions to address this issue.  I am not claiming that only these two 
	 *   methods are available.  There are many, these are quick and easy to implement as per my knowedge. 
	 */
	public function randomArrayOperation($min, $max, $weight)
	{

		$numbers = range($min, $max);
		shuffle($numbers);
		$randomArrayWithWeight =  array_slice($numbers, 0, $weight);

		/**
		 * || Developer comments - Method - 1 ||
		 * Perhaps this method can best used when we have to find multiple missing values
		 */

		$randomArrayWithRemovedItem = $randomArrayWithWeight; //Copy just to compare.
		$remElement 				= rand($min, $max);
		array_splice($randomArrayWithRemovedItem,$remElement,1); //Discard one arbitary item

		echo "Generated 500 size array:";
		$this->printr($randomArrayWithWeight);

        echo "Structure of the array after discarding an arbitraty item:";
        $this->printr($randomArrayWithRemovedItem);	

		echo "Items missing from the second array:";
		$this->printr($arrListOfMissing);

		/**
		 * || Developer comments - Method - 2 ||
		 * Considering only the given question, we can adapt the following approach also
		 */
		unset( $randomArrayWithWeight[$remElement] );
        $removedElement = array_sum(range($min,$max)) - array_sum($randomArrayWithWeight);
        
        
         // ### Uncomment the follwing lines of code to see the output of above soltuion. #### 	 

		/*
        echo "Structure of the array after discarding an arbitraty item:";
        $this->printr($randomArrayWithRemovedItem);	

		echo "Item removed the array is : $removedElement";		
		*/
	}


	/**
	 * 3. Database Connectivity 
	 * Demonstrate with PHP how you would connect to a MySQL (InnoDB) database and 
	 * query for all records with the following fields: (name, age, job_title) from a 
	 * table called 'exads_test'. 
	 * 
	 * Also provide an example of how you would write a sanitised record to the same table. 
	 *
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ In this question the term "sanitised" has been used, hence I am assuming the imporantance of SQL
	 *   injections.  "Prepared statements are resilient against SQL injection" and hence I am opting
	 *   PDO over MySQLi.
	 */

	public function theDatabaseConnectivity($host, $db, $username, $password)
	{
		/**
		 * I have placed multiple try/catch because on a real application , each block will be called seperatyl
		 * In this case, just one try/catch will be enough
		 */

		$dsn = "mysql:host=$host;dbname=$db;charset=utf8mb4";

		$options = [
			PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array, because in previous question my array management skill was checked, so assuming array is an important factor.
		];

		try {
			$pdo = new PDO($dsn, $username, $password, $options);
		} catch (Exception $e) {
			error_log($e->getMessage());
			exit('Unable connect to the Database !!'); //something a user can understand
		}

		// Just for a sorted data list I have applied an Order by.
		$pdoStmnt 		= $pdo->prepare("SELECT name, age, job_title FROM exads_test ORDER BY name");
		$pdoStmnt->execute();
		$arrTestList 	= $pdoStmnt->fetchAll(PDO::FETCH_ASSOC);

		if(!$arrTestList) exit('No records found!');
		$this->printr($arrTestList);


		//Mostly request data from a form or whatever
		$arrDataToInsert 	= [
			'Aiden',
			'35',
			'Software Engineer'
		];

		//As only three fields, I have used '?' instead of placeholder labels like ':name'
		$sqlInsert 			= "INSERT INTO exads_test (name, age, job_title) VALUES (?,?,?)"; 
		$pdoStmnt 			= $pdo->prepare($sqlInsert) ;

		//We can also manage exceptions here using a try/catch if needed.
		try{
			$pdoStmnt->execute($arrDataToInsert);
		} catch( PDOException $e){		
			echo "Unexpected error.  #", $e->getMessage(), "<br>";
		}
	}

	/**	 	
	 * 4. Date Calculation
	 * The Irish National Lottery draw takes place twice weekly on a Wednesday and a Saturday at 8pm.
	 * 
	 * Write a function or class that calculates and returns the next valid draw date based on the 
	 * current date and time AND also on an optionally supplied date and time.
	 *
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ As an emphasis given on the time 8 PM I have strictly checked the time during the next draw date find.
	 */

	public function theIrishNationalLotteryDraw($datetime = '')
	{

		if( '' != $datetime ){
			$objDate = new DateTime($datetime);	
		} else {
			$objDate = new DateTime();	
		}		

		//this vs next
		$dateToday 		= clone $objDate;
		$dateToday->modify('this day 20:00');		
		$period = ( $objDate->format('Y-m-d h:i:s') >= $dateToday->format('Y-m-d h:i:s') ) ? 'next' : 'this';
		
		
		//Wednesday Date Object
		$dateWednesday 	= clone $objDate;
		$dateWednesday->modify( "$period Wednesday 20:00" );
		

		//Saturday Date object
		$dateSaturday 	= clone $objDate;
		$dateSaturday->modify(  "$period Saturday 20:00" );		

		$hoursToWednesday	= $objDate->diff($dateWednesday);
		$hoursToSaturday	= $objDate->diff($dateSaturday);

		echo  ( $dateWednesday >  $dateSaturday ) ?  $dateSaturday->format('Y-m-d') : $dateWednesday->format('Y-m-d'), "<br>";
	}


	/**	 	
	 * 4. A/B Testing
	 * Exads would like to A/B test a number of promotional designs to see which provides the best 
	 * conversion rate.
	 *
	 * i.e. - 50% of people will be shown Design A, 25% shown Design B and 25% shown Design C.
	 * The code needs to be scalable as a single promotion may have upwards of 3 designs to test. 
	 *
	 *
	 * Reference: ExadsDev-PHPTest.pdf
	 * @ A simple A/B testing load is done with total number of expected outcomes.  However here
	 * her the split_percent has given hence the loading should be done based on that.
	 */
	public function performABTesting()
	{

		// The list of designs - normally loaded from a Database or any other datasource.  
		$arrDataSourceDesigns = [
			[
				'design_id' 	=> '1',
				'design_name'   => 'Design 1',
				'split_percent' => '50'
			],
			[
				'design_id' 	=> '2',
				'design_name'   => 'Design 2',
				'split_percent' => '25'
			],
			[
				'design_id' 	=> '3',
				'design_name'   => 'Design 3',
				'split_percent' => '25'
			],						
		];

		//Obviously foreach is faster.  array_reduce - Just used to make the code look a bit different.
		//Always better to get the sum from the Database, rather than looping list of item and get the sum.
		$iSplitPercentEndRange 	= 	array_reduce($arrDataSourceDesigns, function(&$res, $item) {
		    return $res + $item['split_percent'];
		}, 0);		

		$iProbablityRange 		= rand(1, $iSplitPercentEndRange);
		$iDesignProbalitySum 	= 0;
		$sDesignToRender 		= '';

        foreach ($arrDataSourceDesigns as $designItem ) {
            $iDesignProbalitySum += $designItem ['split_percent'];
            if ($iDesignProbalitySum >= $iProbablityRange) {
                $sDesignToRender = $designItem['design_name'];
                break;
            }
        }

        echo $sDesignToRender;
	}

	public function printr($data){
		echo '<pre>'; print_r($data); echo '</pre>';
	}
}


/**
 * ## Test Execution ##
 */


//Script Execution codes
$objExadsPhpTest = new ExadsDevPhpTest();

// 1. FizzBuzz
$objExadsPhpTest->runFizzBuzz();

// 1. FizzBuzz, Same output, but developed in a different way.
array_map(array($objExadsPhpTest, 'runFizzBuzzImproved'), range(1, 100));

//2. 500 Element Array
$objExadsPhpTest->randomArrayOperation(1,10, 10);

//3. Database Connectivity
$objExadsPhpTest->theDatabaseConnectivity();

//4. Date Calculation
$objExadsPhpTest->theIrishNationalLotteryDraw();
$objExadsPhpTest->theIrishNationalLotteryDraw('2020-01-18 07:00');
$objExadsPhpTest->theIrishNationalLotteryDraw('2020-01-18 08:02');
$objExadsPhpTest->theIrishNationalLotteryDraw('2020-01-24');

//5. A/B Testing
$objExadsPhpTest->performABTesting();